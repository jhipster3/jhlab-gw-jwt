/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ag04.jhlab.gw.web.rest.vm;
