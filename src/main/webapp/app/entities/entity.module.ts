import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'country',
        loadChildren: () => import('./geodataService/country/country.module').then(m => m.GeodataServiceCountryModule),
      },
      {
        path: 'currency',
        loadChildren: () => import('./geodataService/currency/currency.module').then(m => m.GeodataServiceCurrencyModule),
      },
      {
        path: 'financial-institution',
        loadChildren: () =>
          import('./fidataService/financial-institution/financial-institution.module').then(m => m.FidataServiceFinancialInstitutionModule),
      },
      {
        path: 'bic-code',
        loadChildren: () => import('./fidataService/bic-code/bic-code.module').then(m => m.FidataServiceBicCodeModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class JhlabGatewayEntityModule {}
