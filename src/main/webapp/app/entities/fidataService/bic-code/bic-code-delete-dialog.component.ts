import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBicCode } from 'app/shared/model/fidataService/bic-code.model';
import { BicCodeService } from './bic-code.service';

@Component({
  templateUrl: './bic-code-delete-dialog.component.html',
})
export class BicCodeDeleteDialogComponent {
  bicCode?: IBicCode;

  constructor(protected bicCodeService: BicCodeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.bicCodeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bicCodeListModification');
      this.activeModal.close();
    });
  }
}
