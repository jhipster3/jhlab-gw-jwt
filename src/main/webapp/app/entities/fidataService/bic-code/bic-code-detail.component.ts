import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBicCode } from 'app/shared/model/fidataService/bic-code.model';

@Component({
  selector: 'jhi-bic-code-detail',
  templateUrl: './bic-code-detail.component.html',
})
export class BicCodeDetailComponent implements OnInit {
  bicCode: IBicCode | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bicCode }) => (this.bicCode = bicCode));
  }

  previousState(): void {
    window.history.back();
  }
}
