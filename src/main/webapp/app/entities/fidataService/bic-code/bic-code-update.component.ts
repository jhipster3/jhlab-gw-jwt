import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBicCode, BicCode } from 'app/shared/model/fidataService/bic-code.model';
import { BicCodeService } from './bic-code.service';
import { ICountry } from 'app/shared/model/geodataService/country.model';
import { CountryService } from 'app/entities/geodataService/country/country.service';
import { IFinancialInstitution } from 'app/shared/model/fidataService/financial-institution.model';
import { FinancialInstitutionService } from 'app/entities/fidataService/financial-institution/financial-institution.service';

type SelectableEntity = ICountry | IFinancialInstitution;

@Component({
  selector: 'jhi-bic-code-update',
  templateUrl: './bic-code-update.component.html',
})
export class BicCodeUpdateComponent implements OnInit {
  isSaving = false;
  countries: ICountry[] = [];
  financialinstitutions: IFinancialInstitution[] = [];

  editForm = this.fb.group({
    id: [],
    swiftCode: [],
    city: [],
    branch: [],
    address: [],
    postalCode: [],
    bicLat: [],
    bicLng: [],
    country: [],
    financialInstitution: [],
  });

  constructor(
    protected bicCodeService: BicCodeService,
    protected countryService: CountryService,
    protected financialInstitutionService: FinancialInstitutionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bicCode }) => {
      this.updateForm(bicCode);

      this.countryService.query().subscribe((res: HttpResponse<ICountry[]>) => (this.countries = res.body || []));

      this.financialInstitutionService
        .query()
        .subscribe((res: HttpResponse<IFinancialInstitution[]>) => (this.financialinstitutions = res.body || []));
    });
  }

  updateForm(bicCode: IBicCode): void {
    this.editForm.patchValue({
      id: bicCode.id,
      swiftCode: bicCode.swiftCode,
      city: bicCode.city,
      branch: bicCode.branch,
      address: bicCode.address,
      postalCode: bicCode.postalCode,
      bicLat: bicCode.bicLat,
      bicLng: bicCode.bicLng,
      country: bicCode.country,
      financialInstitution: bicCode.financialInstitution,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bicCode = this.createFromForm();
    if (bicCode.id !== undefined) {
      this.subscribeToSaveResponse(this.bicCodeService.update(bicCode));
    } else {
      this.subscribeToSaveResponse(this.bicCodeService.create(bicCode));
    }
  }

  private createFromForm(): IBicCode {
    return {
      ...new BicCode(),
      id: this.editForm.get(['id'])!.value,
      swiftCode: this.editForm.get(['swiftCode'])!.value,
      city: this.editForm.get(['city'])!.value,
      branch: this.editForm.get(['branch'])!.value,
      address: this.editForm.get(['address'])!.value,
      postalCode: this.editForm.get(['postalCode'])!.value,
      bicLat: this.editForm.get(['bicLat'])!.value,
      bicLng: this.editForm.get(['bicLng'])!.value,
      country: this.editForm.get(['country'])!.value,
      financialInstitution: this.editForm.get(['financialInstitution'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBicCode>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
