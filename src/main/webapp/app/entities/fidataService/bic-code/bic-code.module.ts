import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhlabGatewaySharedModule } from 'app/shared/shared.module';
import { BicCodeComponent } from './bic-code.component';
import { BicCodeDetailComponent } from './bic-code-detail.component';
import { BicCodeUpdateComponent } from './bic-code-update.component';
import { BicCodeDeleteDialogComponent } from './bic-code-delete-dialog.component';
import { bicCodeRoute } from './bic-code.route';

@NgModule({
  imports: [JhlabGatewaySharedModule, RouterModule.forChild(bicCodeRoute)],
  declarations: [BicCodeComponent, BicCodeDetailComponent, BicCodeUpdateComponent, BicCodeDeleteDialogComponent],
  entryComponents: [BicCodeDeleteDialogComponent],
})
export class FidataServiceBicCodeModule {}
