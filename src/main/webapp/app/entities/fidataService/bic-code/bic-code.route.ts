import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBicCode, BicCode } from 'app/shared/model/fidataService/bic-code.model';
import { BicCodeService } from './bic-code.service';
import { BicCodeComponent } from './bic-code.component';
import { BicCodeDetailComponent } from './bic-code-detail.component';
import { BicCodeUpdateComponent } from './bic-code-update.component';

@Injectable({ providedIn: 'root' })
export class BicCodeResolve implements Resolve<IBicCode> {
  constructor(private service: BicCodeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBicCode> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bicCode: HttpResponse<BicCode>) => {
          if (bicCode.body) {
            return of(bicCode.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BicCode());
  }
}

export const bicCodeRoute: Routes = [
  {
    path: '',
    component: BicCodeComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'jhlabGatewayApp.fidataServiceBicCode.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BicCodeDetailComponent,
    resolve: {
      bicCode: BicCodeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'jhlabGatewayApp.fidataServiceBicCode.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BicCodeUpdateComponent,
    resolve: {
      bicCode: BicCodeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'jhlabGatewayApp.fidataServiceBicCode.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BicCodeUpdateComponent,
    resolve: {
      bicCode: BicCodeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'jhlabGatewayApp.fidataServiceBicCode.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
