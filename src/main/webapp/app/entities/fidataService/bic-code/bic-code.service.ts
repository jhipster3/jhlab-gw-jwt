import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBicCode } from 'app/shared/model/fidataService/bic-code.model';

type EntityResponseType = HttpResponse<IBicCode>;
type EntityArrayResponseType = HttpResponse<IBicCode[]>;

@Injectable({ providedIn: 'root' })
export class BicCodeService {
  public resourceUrl = SERVER_API_URL + 'services/fidataservice/api/bic-codes';

  constructor(protected http: HttpClient) {}

  create(bicCode: IBicCode): Observable<EntityResponseType> {
    return this.http.post<IBicCode>(this.resourceUrl, bicCode, { observe: 'response' });
  }

  update(bicCode: IBicCode): Observable<EntityResponseType> {
    return this.http.put<IBicCode>(this.resourceUrl, bicCode, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBicCode>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBicCode[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
