import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFinancialInstitution } from 'app/shared/model/fidataService/financial-institution.model';
import { FinancialInstitutionService } from './financial-institution.service';

@Component({
  templateUrl: './financial-institution-delete-dialog.component.html',
})
export class FinancialInstitutionDeleteDialogComponent {
  financialInstitution?: IFinancialInstitution;

  constructor(
    protected financialInstitutionService: FinancialInstitutionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.financialInstitutionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('financialInstitutionListModification');
      this.activeModal.close();
    });
  }
}
