import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFinancialInstitution } from 'app/shared/model/fidataService/financial-institution.model';

@Component({
  selector: 'jhi-financial-institution-detail',
  templateUrl: './financial-institution-detail.component.html',
})
export class FinancialInstitutionDetailComponent implements OnInit {
  financialInstitution: IFinancialInstitution | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ financialInstitution }) => (this.financialInstitution = financialInstitution));
  }

  previousState(): void {
    window.history.back();
  }
}
