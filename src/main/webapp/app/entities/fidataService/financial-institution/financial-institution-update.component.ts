import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFinancialInstitution, FinancialInstitution } from 'app/shared/model/fidataService/financial-institution.model';
import { FinancialInstitutionService } from './financial-institution.service';
import { ICountry } from 'app/shared/model/geodataService/country.model';
import { CountryService } from 'app/entities/geodataService/country/country.service';

@Component({
  selector: 'jhi-financial-institution-update',
  templateUrl: './financial-institution-update.component.html',
})
export class FinancialInstitutionUpdateComponent implements OnInit {
  isSaving = false;
  countries: ICountry[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    description: [],
    address: [],
    address2: [],
    city: [],
    state: [],
    postalCode: [],
    country: [],
  });

  constructor(
    protected financialInstitutionService: FinancialInstitutionService,
    protected countryService: CountryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ financialInstitution }) => {
      this.updateForm(financialInstitution);

      this.countryService.query().subscribe((res: HttpResponse<ICountry[]>) => (this.countries = res.body || []));
    });
  }

  updateForm(financialInstitution: IFinancialInstitution): void {
    this.editForm.patchValue({
      id: financialInstitution.id,
      name: financialInstitution.name,
      description: financialInstitution.description,
      address: financialInstitution.address,
      address2: financialInstitution.address2,
      city: financialInstitution.city,
      state: financialInstitution.state,
      postalCode: financialInstitution.postalCode,
      country: financialInstitution.country,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const financialInstitution = this.createFromForm();
    if (financialInstitution.id !== undefined) {
      this.subscribeToSaveResponse(this.financialInstitutionService.update(financialInstitution));
    } else {
      this.subscribeToSaveResponse(this.financialInstitutionService.create(financialInstitution));
    }
  }

  private createFromForm(): IFinancialInstitution {
    return {
      ...new FinancialInstitution(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      address: this.editForm.get(['address'])!.value,
      address2: this.editForm.get(['address2'])!.value,
      city: this.editForm.get(['city'])!.value,
      state: this.editForm.get(['state'])!.value,
      postalCode: this.editForm.get(['postalCode'])!.value,
      country: this.editForm.get(['country'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFinancialInstitution>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICountry): any {
    return item.id;
  }
}
