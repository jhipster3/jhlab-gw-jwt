import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhlabGatewaySharedModule } from 'app/shared/shared.module';
import { FinancialInstitutionComponent } from './financial-institution.component';
import { FinancialInstitutionDetailComponent } from './financial-institution-detail.component';
import { FinancialInstitutionUpdateComponent } from './financial-institution-update.component';
import { FinancialInstitutionDeleteDialogComponent } from './financial-institution-delete-dialog.component';
import { financialInstitutionRoute } from './financial-institution.route';

@NgModule({
  imports: [JhlabGatewaySharedModule, RouterModule.forChild(financialInstitutionRoute)],
  declarations: [
    FinancialInstitutionComponent,
    FinancialInstitutionDetailComponent,
    FinancialInstitutionUpdateComponent,
    FinancialInstitutionDeleteDialogComponent,
  ],
  entryComponents: [FinancialInstitutionDeleteDialogComponent],
})
export class FidataServiceFinancialInstitutionModule {}
