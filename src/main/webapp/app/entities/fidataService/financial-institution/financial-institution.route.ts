import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFinancialInstitution, FinancialInstitution } from 'app/shared/model/fidataService/financial-institution.model';
import { FinancialInstitutionService } from './financial-institution.service';
import { FinancialInstitutionComponent } from './financial-institution.component';
import { FinancialInstitutionDetailComponent } from './financial-institution-detail.component';
import { FinancialInstitutionUpdateComponent } from './financial-institution-update.component';

@Injectable({ providedIn: 'root' })
export class FinancialInstitutionResolve implements Resolve<IFinancialInstitution> {
  constructor(private service: FinancialInstitutionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFinancialInstitution> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((financialInstitution: HttpResponse<FinancialInstitution>) => {
          if (financialInstitution.body) {
            return of(financialInstitution.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FinancialInstitution());
  }
}

export const financialInstitutionRoute: Routes = [
  {
    path: '',
    component: FinancialInstitutionComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'jhlabGatewayApp.fidataServiceFinancialInstitution.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FinancialInstitutionDetailComponent,
    resolve: {
      financialInstitution: FinancialInstitutionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'jhlabGatewayApp.fidataServiceFinancialInstitution.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FinancialInstitutionUpdateComponent,
    resolve: {
      financialInstitution: FinancialInstitutionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'jhlabGatewayApp.fidataServiceFinancialInstitution.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FinancialInstitutionUpdateComponent,
    resolve: {
      financialInstitution: FinancialInstitutionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'jhlabGatewayApp.fidataServiceFinancialInstitution.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
