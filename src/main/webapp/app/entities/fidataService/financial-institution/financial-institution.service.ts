import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFinancialInstitution } from 'app/shared/model/fidataService/financial-institution.model';

type EntityResponseType = HttpResponse<IFinancialInstitution>;
type EntityArrayResponseType = HttpResponse<IFinancialInstitution[]>;

@Injectable({ providedIn: 'root' })
export class FinancialInstitutionService {
  public resourceUrl = SERVER_API_URL + 'services/fidataservice/api/financial-institutions';

  constructor(protected http: HttpClient) {}

  create(financialInstitution: IFinancialInstitution): Observable<EntityResponseType> {
    return this.http.post<IFinancialInstitution>(this.resourceUrl, financialInstitution, { observe: 'response' });
  }

  update(financialInstitution: IFinancialInstitution): Observable<EntityResponseType> {
    return this.http.put<IFinancialInstitution>(this.resourceUrl, financialInstitution, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFinancialInstitution>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFinancialInstitution[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
