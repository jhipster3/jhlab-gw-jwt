import { ICountry } from 'app/shared/model/geodataService/country.model';
import { IFinancialInstitution } from 'app/shared/model/fidataService/financial-institution.model';

export interface IBicCode {
  id?: number;
  swiftCode?: string;
  city?: string;
  branch?: string;
  address?: string;
  postalCode?: string;
  bicLat?: number;
  bicLng?: number;
  country?: ICountry;
  financialInstitution?: IFinancialInstitution;
}

export class BicCode implements IBicCode {
  constructor(
    public id?: number,
    public swiftCode?: string,
    public city?: string,
    public branch?: string,
    public address?: string,
    public postalCode?: string,
    public bicLat?: number,
    public bicLng?: number,
    public country?: ICountry,
    public financialInstitution?: IFinancialInstitution
  ) {}
}
