import { IBicCode } from 'app/shared/model/fidataService/bic-code.model';
import { ICountry } from 'app/shared/model/geodataService/country.model';

export interface IFinancialInstitution {
  id?: number;
  name?: string;
  description?: string;
  address?: string;
  address2?: string;
  city?: string;
  state?: string;
  postalCode?: string;
  bicCodes?: IBicCode[];
  country?: ICountry;
}

export class FinancialInstitution implements IFinancialInstitution {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public address?: string,
    public address2?: string,
    public city?: string,
    public state?: string,
    public postalCode?: string,
    public bicCodes?: IBicCode[],
    public country?: ICountry
  ) {}
}
