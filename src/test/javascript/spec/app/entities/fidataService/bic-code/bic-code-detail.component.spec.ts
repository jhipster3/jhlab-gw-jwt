import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhlabGatewayTestModule } from '../../../../test.module';
import { BicCodeDetailComponent } from 'app/entities/fidataService/bic-code/bic-code-detail.component';
import { BicCode } from 'app/shared/model/fidataService/bic-code.model';

describe('Component Tests', () => {
  describe('BicCode Management Detail Component', () => {
    let comp: BicCodeDetailComponent;
    let fixture: ComponentFixture<BicCodeDetailComponent>;
    const route = ({ data: of({ bicCode: new BicCode(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhlabGatewayTestModule],
        declarations: [BicCodeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BicCodeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BicCodeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bicCode on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bicCode).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
