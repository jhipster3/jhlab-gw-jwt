import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { JhlabGatewayTestModule } from '../../../../test.module';
import { BicCodeUpdateComponent } from 'app/entities/fidataService/bic-code/bic-code-update.component';
import { BicCodeService } from 'app/entities/fidataService/bic-code/bic-code.service';
import { BicCode } from 'app/shared/model/fidataService/bic-code.model';

describe('Component Tests', () => {
  describe('BicCode Management Update Component', () => {
    let comp: BicCodeUpdateComponent;
    let fixture: ComponentFixture<BicCodeUpdateComponent>;
    let service: BicCodeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhlabGatewayTestModule],
        declarations: [BicCodeUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BicCodeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BicCodeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BicCodeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BicCode(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BicCode();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
