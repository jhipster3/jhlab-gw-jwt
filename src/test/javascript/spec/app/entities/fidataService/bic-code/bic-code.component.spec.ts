import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';

import { JhlabGatewayTestModule } from '../../../../test.module';
import { BicCodeComponent } from 'app/entities/fidataService/bic-code/bic-code.component';
import { BicCodeService } from 'app/entities/fidataService/bic-code/bic-code.service';
import { BicCode } from 'app/shared/model/fidataService/bic-code.model';

describe('Component Tests', () => {
  describe('BicCode Management Component', () => {
    let comp: BicCodeComponent;
    let fixture: ComponentFixture<BicCodeComponent>;
    let service: BicCodeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhlabGatewayTestModule],
        declarations: [BicCodeComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: of({
                defaultSort: 'id,asc',
              }),
              queryParamMap: of(
                convertToParamMap({
                  page: '1',
                  size: '1',
                  sort: 'id,desc',
                })
              ),
            },
          },
        ],
      })
        .overrideTemplate(BicCodeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BicCodeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BicCodeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BicCode(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bicCodes && comp.bicCodes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BicCode(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bicCodes && comp.bicCodes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
