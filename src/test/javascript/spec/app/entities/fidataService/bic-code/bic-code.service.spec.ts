import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BicCodeService } from 'app/entities/fidataService/bic-code/bic-code.service';
import { IBicCode, BicCode } from 'app/shared/model/fidataService/bic-code.model';

describe('Service Tests', () => {
  describe('BicCode Service', () => {
    let injector: TestBed;
    let service: BicCodeService;
    let httpMock: HttpTestingController;
    let elemDefault: IBicCode;
    let expectedResult: IBicCode | IBicCode[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(BicCodeService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new BicCode(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a BicCode', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new BicCode()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a BicCode', () => {
        const returnedFromService = Object.assign(
          {
            swiftCode: 'BBBBBB',
            city: 'BBBBBB',
            branch: 'BBBBBB',
            address: 'BBBBBB',
            postalCode: 'BBBBBB',
            bicLat: 1,
            bicLng: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of BicCode', () => {
        const returnedFromService = Object.assign(
          {
            swiftCode: 'BBBBBB',
            city: 'BBBBBB',
            branch: 'BBBBBB',
            address: 'BBBBBB',
            postalCode: 'BBBBBB',
            bicLat: 1,
            bicLng: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BicCode', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
