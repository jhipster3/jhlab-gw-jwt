import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhlabGatewayTestModule } from '../../../../test.module';
import { FinancialInstitutionDetailComponent } from 'app/entities/fidataService/financial-institution/financial-institution-detail.component';
import { FinancialInstitution } from 'app/shared/model/fidataService/financial-institution.model';

describe('Component Tests', () => {
  describe('FinancialInstitution Management Detail Component', () => {
    let comp: FinancialInstitutionDetailComponent;
    let fixture: ComponentFixture<FinancialInstitutionDetailComponent>;
    const route = ({ data: of({ financialInstitution: new FinancialInstitution(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhlabGatewayTestModule],
        declarations: [FinancialInstitutionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FinancialInstitutionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FinancialInstitutionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load financialInstitution on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.financialInstitution).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
