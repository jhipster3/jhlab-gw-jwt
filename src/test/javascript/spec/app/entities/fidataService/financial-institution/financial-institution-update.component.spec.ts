import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { JhlabGatewayTestModule } from '../../../../test.module';
import { FinancialInstitutionUpdateComponent } from 'app/entities/fidataService/financial-institution/financial-institution-update.component';
import { FinancialInstitutionService } from 'app/entities/fidataService/financial-institution/financial-institution.service';
import { FinancialInstitution } from 'app/shared/model/fidataService/financial-institution.model';

describe('Component Tests', () => {
  describe('FinancialInstitution Management Update Component', () => {
    let comp: FinancialInstitutionUpdateComponent;
    let fixture: ComponentFixture<FinancialInstitutionUpdateComponent>;
    let service: FinancialInstitutionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhlabGatewayTestModule],
        declarations: [FinancialInstitutionUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FinancialInstitutionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FinancialInstitutionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FinancialInstitutionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FinancialInstitution(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FinancialInstitution();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
