import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';

import { JhlabGatewayTestModule } from '../../../../test.module';
import { FinancialInstitutionComponent } from 'app/entities/fidataService/financial-institution/financial-institution.component';
import { FinancialInstitutionService } from 'app/entities/fidataService/financial-institution/financial-institution.service';
import { FinancialInstitution } from 'app/shared/model/fidataService/financial-institution.model';

describe('Component Tests', () => {
  describe('FinancialInstitution Management Component', () => {
    let comp: FinancialInstitutionComponent;
    let fixture: ComponentFixture<FinancialInstitutionComponent>;
    let service: FinancialInstitutionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhlabGatewayTestModule],
        declarations: [FinancialInstitutionComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: of({
                defaultSort: 'id,asc',
              }),
              queryParamMap: of(
                convertToParamMap({
                  page: '1',
                  size: '1',
                  sort: 'id,desc',
                })
              ),
            },
          },
        ],
      })
        .overrideTemplate(FinancialInstitutionComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FinancialInstitutionComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FinancialInstitutionService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FinancialInstitution(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.financialInstitutions && comp.financialInstitutions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FinancialInstitution(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.financialInstitutions && comp.financialInstitutions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
